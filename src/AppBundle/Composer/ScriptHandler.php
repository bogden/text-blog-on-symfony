<?php
namespace AppBundle\Composer;

use Composer\Script\Event;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class ScriptHandler extends \Sensio\Bundle\DistributionBundle\Composer\ScriptHandler {


    /**
     * Call the demo command of the Acme Demo Bundle.
     *
     * @param $event Event A instance
     */
    public static function install(Event $event)
    {
        $options = self::getOptions($event);
        $consoleDir = self::getConsoleDir($event, 'install');
        if (null === $consoleDir) {
            return;
        }
        static::executeCommand($event, $consoleDir, 'doctrine:schema:update --force', $options['process-timeout']);
        static::executeCommand($event, $consoleDir, 'propel:migration:diff', $options['process-timeout']);
        static::executeCommand($event, $consoleDir, 'propel:migration:migrate', $options['process-timeout']);
        static::executeCommand($event, $consoleDir, 'propel:model:build', $options['process-timeout']);
        static::executeCommand($event, $consoleDir, 'propel:fixtures:load ', $options['process-timeout']);

    }

    public static function gulpBuild(Event $event) {

        $process = new Process('bower install && gulp external:build');
        if(!file_exists($process->getWorkingDirectory().'/markup/node_modules/symfony-stydio-markup/index.js')){
            $process = new Process('npm install && bower install && gulp external:build');
        }
        $process
            ->setTimeout(3600)
            ->setWorkingDirectory($process->getWorkingDirectory().'/markup')
            ->run();
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
            $event->getIO()->write($process->getOutput());
        }
        else {
            $event->getIO()->write($process->getOutput());
            $event->getIO()->write("Recompiled and compressed CSS/JS");
        }
    }

}