<?php

namespace AppBundle\Controller;

use AppBundle\Model\Comment;
use AppBundle\Model\Post;
use AppBundle\Model\PostQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use function Symfony\Component\Debug\Tests\testHeader;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\Length;

class PostController extends Controller
{
    /**
     * @Route("/show-post/{slug}/", name="show-post")
     * @ParamConverter("post", converter="propel", class="AppBundle\Model\Post")
     * @param Request $request
     * @param Post|null $post
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function showAction(Request $request, Post $post = null)
    {
        if (!$post) throw new NotFoundHttpException('Post not found');
        return $this->render('Post/post.full.html.twig', [
            'post' => $post,
        ]);
    }

    /**
     * @Route("/new-post/", name="new-post")
     * @Route("/edit-post/{id}/", name="edit-post")
     * @ParamConverter("post", converter="propel", class="AppBundle\Model\Post")
     * @param Request $request
     * @param Post|null $post
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, Post $post = null)
    {
        $translator = $this->get('translator');
        $user = $this->getUser();
        if (!$user || ($request->get('_route') == 'edit-post' && $post->getAuthorId() != $this->getUser()->getId()))
            throw new NotFoundHttpException('Current user not found');


        $post = $post ? $post : new Post();
        $fileName = $post->getThumbnail(true);
        $filePath = $post->getThumbnailPath();

        if (!$this->isGranted('IS_AUTHENTICATED_REMEMBERED') || (!$post->isNew() && $this->getUser()->getID() != $post->getAuthorId())) {
            throw new AccessDeniedHttpException('Access denied');
        }

        $status_choice = [];
        foreach (Post::$statuses as $key => $status) {
            $status_choice[$translator->trans($status)] = $key;
        }

        $form = $this->createFormBuilder($post)
            ->add('title', TextType::class, ['constraints' => new Length(array('min' => 3)), 'label' => $translator->trans('post.title')])
            ->add('content', TextareaType::class, ['constraints' => new Length(array('min' => 20)), 'label' => $translator->trans('post.content'), 'attr' => ['rows' => '10']])
            ->add('thumbnail', FileType::class, ['label' => $translator->trans('post.thumbnail'),'constraints' => new Image([
                'corruptedMessage' => $translator->trans('post.not image'),
                'mimeTypesMessage' => $translator->trans('post.not image')
            ]), 'required' => false])
            ->add('status', ChoiceType::class, ['choices' => $status_choice, 'label' => $translator->trans('post.statuses')])
            ->add('save', SubmitType::class, array('label' => 'Create Post'))
            ->getForm();
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Post $post */
            $post = $form->getData();

            /** @var UploadedFile|null $file */
            $file = $form->get('thumbnail')->getData();
            if($file){
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                $filePath = $this->getParameter('web_directory').$this->getParameter('upload_path');
                $file
                    ->move($filePath, $fileName);
            }

            $post
                ->setThumbnailPath($filePath)
                ->setThumbnail($fileName)
                ->setAuthorId($user->getId())
                ->save();
            $this->get('session')->getFlashBag()->add('success', $translator->trans('post.add.confirm'));
            return $this->redirectToRoute('homepage');
        }

        return $this->render('Default/add.post.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete-post/", name="delete-post")
     * @Route("/delete-post/{slug}/", name="delete-post-with-slug")
     * @ParamConverter("post", converter="propel", class="AppBundle\Model\Post")
     * @param Request $request
     * @param Post|null $post
     * @return JsonResponse
     */
    public function  deleteAction(Request $request, Post $post = null)
    {
        $post = ($post instanceof Post) ? $post : PostQuery::create()->findOneBySlug($request->request->get('slug'));
        if (!$post) throw new NotFoundHttpException('Post not found');
        if($post->canEdit($this->getUser())){
            $post->delete();
            $data = ['success' => true];
        }else{
            throw new AccessDeniedHttpException();
        }
        return new JsonResponse($data);
    }
}
