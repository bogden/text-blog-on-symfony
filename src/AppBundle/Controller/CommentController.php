<?php

namespace AppBundle\Controller;

use AppBundle\Model\Comment;
use AppBundle\Model\CommentQuery;
use AppBundle\Model\Post;
use AppBundle\Model\PostQuery;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraints\Length;

class CommentController extends Controller
{
    /**
     * @Route("/_comment/delete/", name="comment-delete")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @internal param Post|null $post
     */
    public function deleteAction(Request $request)
    {
        if(!$request->isXmlHttpRequest()) throw new NotFoundHttpException();
        $comment = CommentQuery::create()->findPk($request->request->get('id'));
        if($comment && $comment->isUserCanDelete($this->getUser())){
            $comment->delete();
            $data = ['success' => true];
        }else{
            throw new AccessDeniedHttpException();
        }

        return new JsonResponse($data);
    }

    /**
     * @Route("/_comment/load/", name="comment-load")
     * @Route("/_comment/load/{id}/", name="comment-load-for-post")
     * @ParamConverter("post", converter="propel", class="AppBundle\Model\Post")
     * @param Request $request
     * @param Post $post
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @internal param Post|null $post
     */
    public function loadAction(Request $request, Post $post = null)
    {
        if(!$post) $post = PostQuery::create()->findPk($request->request->get('post_id'));
        if(!$post) throw new NotFoundHttpException();
        return $this->render('Comment/comment.list.twig',['post' => $post]);
    }

    /**
     * @Route("/_comment/form/", name="comment-form")
     * @Route("/_comment/form/{id}", name="comment-form-for-post")
     * @ParamConverter("post", converter="propel", class="AppBundle\Model\Post")
     * @param Request $request
     * @param Post $post
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @internal param Post|null $post
     */
    public function formAction(Request $request, Post $post = null)
    {
        if(!$post) $post = PostQuery::create()->findPk($request->request->get('post_id'));
        if(!$post) throw new NotFoundHttpException();


        $translator = $this->get('translator');
        $user = $this->getUser();

        $comment = new Comment();
        $form = $this->createFormBuilder($comment)
            ->setAction($this->generateUrl('comment-form-for-post',['id'=>$post->getId()]))
            ->setMethod('POST')
            ->add('post_id', HiddenType::class, ['attr' => ['value' => $post->getId()]])
            ->add('content', TextareaType::class, ['constraints' => new Length(array('min' => 10)), 'label' => $translator->trans('post.content'), 'attr' => ['rows' => '10']])
            ->add('save', SubmitType::class, array('label' => $translator->trans('comment.add button')))
            ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Comment $comment */
            $comment = $form->getData();
            $comment
                ->setPost($post)
                ->setAuthorId($user->getId())
                ->save();
        }

        if ($form->isSubmitted()) {
            $data = [];
            if($form->isValid()){
                $data['success'] = true;
                $data['message'] = $translator->trans('comment.added');
                $data['clear_form'] = true;
                $data['redirect'] = '';
                $data['reload_block'] = 'comment-list';

            }else{
                foreach ($form->all() as $child) {
                    if (!$child->isValid()) {
                        dump($form->getErrors(true, false));
                        $data['error'][$child->getName()][] = $form->getErrors($child)->__toString();
                    }
                }
            }
            if($request->isXmlHttpRequest()){
                return new JsonResponse($data);}
            else{
                $this->get('session')->getFlashBag()->add('success', $translator->trans('comment.added'));
                return new RedirectResponse($request->headers->get('referer'));
            }
        }

        return $this->render('Comment/comment.form.twig',['form' => $form->createView()]);
    }


}
