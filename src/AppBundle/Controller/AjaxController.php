<?php

namespace AppBundle\Controller;

use AppBundle\Model\Comment;
use AppBundle\Model\Post;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use function Symfony\Component\Debug\Tests\testHeader;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraints\Length;

class AjaxController extends Controller
{
    /**
     * @Route("/_ajax/get_route/{name}/", name="get-route-path")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @internal param Post|null $post
     */
    public function routeAction(Request $request, $name = null)
    {
        if(!$request->isXmlHttpRequest() || !$name) throw new NotFoundHttpException();
        $data = $request->request->all();
        return new JsonResponse(['url' => $this->generateUrl($name, $data)]);
    }

}
