<?php

namespace AppBundle\Model;

use AppBundle\Entity\User;
use AppBundle\Model\Base\Post as BasePost;
use AppBundle\Utils\Formatter;
use AppBundle\Utils\WebFile;
use Propel\Runtime\Connection\ConnectionInterface;

/**
 * Skeleton subclass for representing a row from the 'post' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Post extends BasePost
{
    const STATUS_DRAFT = 0;
    const STATUS_PUBLIC = 1;

    static public $statuses = [
        self::STATUS_DRAFT => 'post.status.draft',
        self::STATUS_PUBLIC => 'post.status.public'
    ];

    public function preSave(ConnectionInterface $con = null)
    {
        if(!$this->getSlug()) $this->setSlug(Formatter::transliterate($this->getTitle()));
        return parent::preSave($con);
    }

    public function getThumbnail($string = false)
    {
        if($string) return $this->thumbnail;
        return $this->thumbnail ? new WebFile($this->thumbnail_path.$this->thumbnail) : null;
    }

    public function canEdit(User $user = null)
    {
        if(!$user) return false;
        return $user->getId() == $this->getAuthorId();
    }
}
