<?php

namespace AppBundle\Model;

use AppBundle\Entity\User;
use AppBundle\Model\Base\PostQuery as BasePostQuery;
use Propel\Runtime\ActiveQuery\Criteria;

/**
 * Skeleton subclass for performing query and update operations on the 'post' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class PostQuery extends BasePostQuery
{
    public function filterByProd(User $user = null)
    {
        $this->orderByCreatedAt(Criteria::DESC);
        $this->where('Post.Status = ?', Post::STATUS_PUBLIC);
        if($user)
            $this->_or()
            ->where('Post.AuthorId = ?', $user->getId());
        return $this;
    }
}
