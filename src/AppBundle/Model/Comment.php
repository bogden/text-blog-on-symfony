<?php

namespace AppBundle\Model;

use AppBundle\Entity\User;
use AppBundle\Model\Base\Comment as BaseComment;

/**
 * Skeleton subclass for representing a row from the 'comment' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Comment extends BaseComment
{
    public function isUserCanDelete(User $user = null)
    {
        if($user && $this->getAuthorId() == $user->getId()) return true;
        return false;
    }
}
