<?php

namespace AppBundle\Service;

use AppBundle\Model\Post;
use AppBundle\Model\PostQuery;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PostService
{

    protected $container;

    /**
     * PostService constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param array $args
     * @return \AppBundle\Model\Post[]|null|\Propel\Runtime\Collection\ObjectCollection|\Propel\Runtime\Util\PropelModelPager
     */
    public function getPosts($args = [])
    {
        if (is_array($args)) $args = array_merge($args, [
            'page' => 1
        ]); else return null;
        $posts = PostQuery::create()->filterByProd($this->container->get('security.token_storage')->getToken()->getUser());
        if ($args['pagination'] > 0 && $args['page'] > 0) return $posts->paginate($args['page'], $args['pagination']);
        return $posts->find();

    }

    public function getPostComments(Post $post = null){
        if(!$post) throw new NotFoundHttpException('Post not fount (comment service)');
        return $post->getComments();
    }

    /**
     * @param $data
     */
    public function savePost($data)
    {

    }
}