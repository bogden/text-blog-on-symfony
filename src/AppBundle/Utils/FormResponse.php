<?php

namespace AppBundle\Utils;

use Symfony\Component\HttpFoundation\ParameterBag;

class FormResponse
{

    public $data;
    protected $schema;
    protected $error;
    protected $success;

    public function __construct()
    {
        $this->data = new ParameterBag();
        $this->error = new ParameterBag();
    }

    public function error($message, $scope = '#')
    {
        $error = $this->error->get($scope);

        if(null === $error){
            $error = [];
        }

        $error[] = $message;

        $this->error->set($scope, $error);

        return $this;
    }

    public function flushError($message = null, $scope = '#'){
        if(null !== $message){
            $this->error($message, $scope);
        }
        if($this->hasError()){
            throw new FormException();
        }
    }

    public function hasError(){
        return !!$this->error->count();
    }

    public function dump()
    {
        if($this->error->count()){
            return ['error' => $this->error->all()];
        }else{
            return $this->data->all();
        }
    }

}