<?php

namespace AppBundle\Utils;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class FormHandler
{

    /** @var FormFactory */
    protected $formFactory;
    /** @var Form */
    protected $form;
    /** @var callable */
    protected $process;
    /** @var callable */
    protected $fail;

    protected $collections = [];

    protected $container;
    protected $region;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
        $this->formFactory = $this->container->get('form.factory');
        $this->form = $this->formFactory
            ->createNamedBuilder('', FormType::class, null, ['allow_extra_fields' => true, 'csrf_protection' => false])
            ->getForm();
    }

    public function add($child, $options = []){
        if(is_array($child)){
            $childName = key($child);
            $this->collections[$childName] = $child[$childName];
            $child = $childName;
        }
        $this->form->add($child, null, $options);

        return $this;
    }


    public function process(callable $callable){
        $this->process = $callable;
        return $this;
    }

    public function fail(callable $callable){
        $this->fail = $callable;
        return $this;
    }


    public function handleCollection($data, FormResponse $response, $prefix = null)
    {
        $this->form->submit($data);

        if(!$this->form->isValid()){
            foreach($this->form as $fieldName => $field){
                foreach ($field->getErrors(true) as $error) {
                    $response->error($error->getMessage(), null === $prefix ? $fieldName : "{$prefix}[{$fieldName}]");
                }
            }
        }
        
        return $this->form->getData();
    }
    
    protected function response(){
        $response = new FormResponse();
        $jsonResponse = new JsonResponse();

        try{

            if(!$this->form->isSubmitted()){
                $response->flushError('Ошибка отправки формы');
            }

            if($message = $this->form->getErrors()->current()){
                $response->error($message);
            }

            foreach($this->form as $fieldName => $field){
                foreach ($field->getErrors(true) as $error) {
                    $response->error($error->getMessage(), $fieldName);
                }
            }

            $data = $this->form->getData();

            foreach($this->collections as $fieldName => $innerFields){
                $field = $this->form->get($fieldName);
                $form = $this->container->get('app.form');
                foreach($innerFields as $innerFieldName => $innerFieldOptions){
                    $form->add($innerFieldName, $innerFieldOptions);
                }
                $data[$fieldName] = $form->handleCollection($field->getData(), $response, $fieldName);
            }

            $response->flushError();

            if($callable = $this->process) {
                if($result = $callable($data, $response)){
                    return $result;
                }
                $response->flushError();
            }

        } catch (FormException $e){

            if($callable = $this->fail){
                if($result = $callable($response->dump())){
                    return $result;
                }
            }

            return $jsonResponse->setData($response->dump());
        }

        return $jsonResponse->setData(array_merge(['success' => true], $response->dump()));
    }


    /**
     * @param null|Request|array|boolean $data
     * @return Response
     */
    public function handle($data = null){
        if(null === $data){
            $data = $this->container->get('request_stack')->getMasterRequest();
        }

        if($data instanceof Request){
            return $this->handleRequest($data);
        }else{
            return $this->handleData($data === true ? [] : $data);
        }
    }

    public function handleRequest(Request $request){
        $this->form->handleRequest($request);
        return $this->response();
    }

    public function handleData(array $data)
    {
        $this->form->submit($data);
        return $this->response();
    }

}
