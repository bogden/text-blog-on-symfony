(function () {
    /**
     * Хэлпер с глобальными функциями
     */
    var Lib = {
        window: $(window),
        browser: (function () {
            var ua = navigator.userAgent;
            var tem;
            var M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
            var assign = function (obj) {
                $('html').addClass(obj.name + ' ' + obj.name + obj.version);
                return obj;
            };
            if (/trident/i.test(M[1])) {
                tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
                return assign({
                    name: 'ie',
                    version: (tem[1] || '')
                });
            }
            if (M[1] === 'Chrome') {
                tem = ua.match(/\bOPR\/(\d+)/);
                if (tem != null) {
                    return assign({
                        name: 'opera',
                        version: tem[1]
                    });
                }
            }
            M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
            if ((tem = ua.match(/version\/(\d+)/i)) != null) {
                M.splice(1, 1, tem[1]);
            }
            return assign({
                name: M[0].toLowerCase(),
                version: M[1]
            });
        })(),
        isMobile: (function () {
            var test = (/iphone|ipad|ipod|android|blackberry|mini|windowssce|palm/i.test(navigator.userAgent));
            if (test) {
                $(function () {
                    $('html').addClass('is-mobile');
                });
            }
            return test;
        })(),
        uniFormBlackout: function ($overlayParent) {
            var $overlay, staticPosition = $overlayParent.css('position') == 'static';

            if (staticPosition) {
                $overlayParent.css('position', 'relative');
            }

            $overlay = $('<div class="form-overlay"></div>').appendTo($overlayParent);
            $overlay.css({opacity: 0, borderRadius: $overlayParent.css('border-radius')});
            $overlay.animate({opacity: .5}, 200);

            return function () {
                $overlay.remove();

                if (staticPosition) {
                    $overlayParent.css('position', 'static');
                }
            };
        },
        uniFormError: function ($container, errors) {
            if (errors === undefined) {
                $container.find('.form__error:not([data-error-handler])').remove();
                $container.find('.form__error-handler').text('').attr('style', '');
                $container.find('.form__error').removeClass('error');
            }
            else {

                $.each(errors, function (key, value) {
                    if ('#' == key) {

                        if (typeof value == 'object') {
                            $$.alert(value.join('<br>'));
                        } else {
                            $$.alert(value);
                        }

                    }
                    else {

                        var field = $container.find('[name="form[' + key + ']"]'),
                            fieldHandler = field.closest('.form-group').find('[data-error-handler]');
                        console.log(fieldHandler);
                        if (fieldHandler.length) {
                            field = fieldHandler;
                        }

                        if (field.length) {
                            Lib.uniFormShowError(field, value);
                        }
                    }
                });

                var $error = $container.find('div.error:eq(0)');
                if (!Lib.isElementOnView($error)) {
                    Lib.scrollTo($error);
                }
            }
        },
        uniFormShowError: function ($field, message) {
            if (!$field.is(':visible')) {
                //return;
            }
            console.log($field);
            var $error = $('<div class="invalid-feedback">' + message + '</div>'),
                $label = $field.closest('.form-group'),
                $inputs = $label.find('.form-control');

            $field.addClass('is-invalid').prepend($error);
            $inputs.addClass('is-invalid');

            $error.click(function () {
                $inputs[0].focus();
                $field.removeClass('is-invalid');
                $inputs.removeClass('is-invalid');
                $error.remove();
            });

            $inputs.on('focus', function () {
                console.log('dsds');
                $field.removeClass('is-invalid');
                $inputs.removeClass('is-invalid');
                $error.remove();
            });


            $error.hide().fadeIn(100);
        },

        uniFormHandle: function (form, callbacks) {
            form.on('submit', function (e) {
                e.preventDefault();
                Lib.uniFormSubmit($(this), callbacks);
            });
        },
        uniFormSubmit: function (form, callbacks) {
            var blackout;

            if (callbacks.success) {
                callbacks.afterSuccess = callbacks.success;
                delete callbacks.success;
            }

            form.find('.input__error').each(function () {
                $(this).remove();
            });

            if (form.data('blocked')) return;

            if (callbacks.beforeData) {
                var beforeData = callbacks.beforeData(form);
                if (beforeData === false) return false;
            }

            if (callbacks.overlay) {
                blackout = Lib.uniFormBlackout(callbacks.overlay === true ? form : callbacks.overlay);
            }

            form.data('blocked', true);
            var ignoreDefaultLoading = form.data('ignoreDefaultLoading');
            var url = location.protocol + '//' + location.hostname + location.pathname;
            var params = {url: url};

            if (callbacks.data) {
                $.extend(params, callbacks.data);
            }

            if (callbacks.beforeSubmit) callbacks.beforeSubmit(form);


            form.find('.attach-images').each(function () {
                var $root = $(this),
                    $images = $root.find('.attach-images-item'),
                    $input = $root.siblings('.form-input-field').find('textarea, input'),
                    text = $input.val();

                $images.each(function () {
                    var $image = $(this);
                    if ($image.data('id')) {
                        if (!text.match(new RegExp('\\[img=' + $image.data('id') + '\\]', 'ig'))) {
                            text += '[img=' + $image.data('id') + ']';
                        }
                    }
                });

                $input.val(text);
            });

            form.ajaxSubmit({
                dataType: 'json', data: params,
                beforeSend: function () {
                    Lib.uniFormError(form);
                },
                complete: function () {
                    form.data('blocked', false);
                    if (callbacks.overlay) {
                        blackout();
                    }
                },
                success: function (data) {
                    form.data('blocked', false);
                    if (data.error) {
                        Lib.uniFormError(form, data.error);

                        if (callbacks.afterData) callbacks.afterData(form, data);

                    } else if (data.success) {
                        form.find('.attach-images').remove();
                        if (callbacks.afterData) callbacks.afterData(form, data);
                        if (callbacks.afterSuccess) {
                            callbacks.afterSuccess(data, form);
                            form.trigger('success', data);
                        }

                        if (data.$events) {
                            $.each(data.$events, function (event, options) {
                                $$.dispatcher.trigger(event, options);
                            });
                        }

                    } else {
                        if (callbacks.afterData) callbacks.afterData(form, data);
                    }


                },
                error: function (request, type, message) {
                }
            });
            return false;
        },
        createModalBox: function (cssClass, options) {
            return $('<div class="modal-box ' + ( typeof cssClass !== 'object' ? cssClass : '' ) + ' ' + ( (options && options.boxCssClass) ? options.boxCssClass : '' ) + '">' +
                '<i class="arcticmodal-close modal-box-close"></i>' +
                '<div class="modal-box-content"></div>' +
                '');
        },
        openModal: function (className, options) {
            var $content;
            options = options || {};
            if (typeof className === 'object' && className.jquery) {
                var $clone = className.clone();
                $content = $$.createModalBox(className, options)
                    .find('.modal-box-content')
                    .html(options.title ? '<div class="modal-box-title">' + options.title + '</div>' : '')
                    .append($clone)
                    .end();


                $$.init.formPlugins($content);

                return $content.arcticmodal(options);
            }
            else {
                $content = $$.createModalBox(className)
                    .find('.modal-box-content')
                    .html((options.title ? '<div class="modal-box-title">' + options.title + '</div>' : '') + $('.' + className).html())
                    .end();

                $$.init.formPlugins($content);

                return $content.arcticmodal(options);
            }
        },
        morph: function (n, u1, u234, u10) {
            n = parseInt(n);
            n = Math.abs(n) % 100;
            if (n > 10 && n < 20) return u10;
            n = n % 10;
            if (n > 1 && n < 5) return u234;
            if (n == 1) return u1;
            return u10;
        },
        scrollTo: function (elem, speed, offset, callback) {
            if (!elem.length) return false;
            var selector = 'html:not(:animated), body:not(:animated)';
            var blocked = false;
            var $modal = elem.closest('.arcticmodal-container');
            if ($modal.length) {
                selector = $modal.filter(':not(:animated)');
            }

            if (!speed) speed = 300;
            if (!offset) offset = 0;
            var offs = {};
            if (elem - 0 >= 0) {
                offs.top = elem;
            } else {
                offs = elem.offset();
            }

            offset = offs.top - offset;
            // offset = typeof selector == 'string' ? (offset-50) : (offset - $(document).scrollTop()+$(selector).scrollTop() - 30);

            if (offs) {
                $(selector).animate({scrollTop: offset}, speed, function () {
                    if (callback && !blocked) {
                        blocked = true;
                        callback();
                    }
                });
                $(window).one('scroll', function () {
                    $(selector).stop();
                    if (callback && !blocked) {
                        blocked = true;
                        callback();
                    }
                });
            }

            blocked = false;
        },
        isElementOnView: function (elem, offset) {
            if (!elem.length) return false;
            if (!offset) offset = 0;
            var docViewTop = $(window).scrollTop();
            var docViewBottom = docViewTop + $(window).height();

            var elemTop = $(elem).offset().top;
            var elemBottom = elemTop + $(elem).height();

            return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop + offset));
        },
        initPrettyPhoto: function (context) {
            $("a[rel^='prettyPhoto'], a[rel^='image'], a[rel^='media']", context).prettyPhoto({
                animation_speed: 'fast', /* fast/slow/normal */
                opacity: 0.60, /* Value between 0 and 1 */
                show_title: false, /* true/false */
                allow_resize: true, /* Resize the photos bigger than viewport. true/false */
                default_width: 800,
                default_height: 600,
                counter_separator_label: ' |В· ', /* The separator for the gallery counter 1 "of" 2 */
                theme: 'overbyte', /* light_rounded / dark_rounded / light_square / dark_square / facebook */
                horizontal_padding: 20, /* The padding on each side of the picture */
                hideflash: false, /* Hides all the flash object on a page, set to TRUE if flash appears over prettyPhoto */
                wmode: 'opaque', /* Set the flash wmode attribute */
                autoplay: true, /* Automatically start videos: True/False */
                modal: false, /* If set to true, only the close button will close the window */
                deeplinking: false, /* Allow prettyPhoto to update the url to enable deeplinking. */
                overlay_gallery: false, /* If set to true, a gallery will overlay the fullscreen image on mouse over */
                keyboard_shortcuts: false,
                ie6_fallback: true,
                social_tools: false
            });
        },
        confirm: function (params) {
            var message = params.message ? params.message : arguments[0];
            var onConfirm = params.onConfirm ? params.onConfirm : arguments[1];
            var onCancel = params.onCancel ? params.onCancel : arguments[2];

            var $box = $$.createModalBox('modal-box-message');
            var $content = $box.find('.modal-box-content');
            $content.html('<div class="modal-box-tabled"><div class="modal-box-tabled-cell"><div class="modal-box-tabled-cell-body">' + message + '</div></div></div>');
            $('<div class="modal-box-buttons modal-box-buttons--center"><button button="middle" class="button-confirm-action">OK</button><button  button="middle outline" class="button-cancel-action arcticmodal-close">Отмена</button></div></div>').insertAfter($content);
            $box.arcticmodal();
            $box.find('.button-confirm-action')[0].focus();
            if (onCancel) $box.find('.button-cancel-action').on('click', function () {
                onCancel()
            });
            $box.find('.button-confirm-action').on('click', function () {
                $box.arcticmodal('close');
                if (onConfirm) onConfirm();
            });
        },
        alert: function (text, params) {

            function _alert(message, params) {
                var $box = $$.createModalBox('modal-box-message modal-box-alert');
                var $content = $box.find('.modal-box-content');
                $content.html('' + message + '<div class="modal-box-buttons modal-box-buttons_center"><button type="button" class="button arcticmodal-close">ОК</button></div>');
                $box.arcticmodal();
                $box.find('button')[0].focus();
                var onOk = params && params.onOk ? params.onOk : arguments[2];
                $box.find('button').on('click', function () {
                    if (onOk) onOk();
                });
            };

            _alert(text, params);
        },
        getRoute: function (route, data) {
            var response = {
                'url': null
            };
            $.ajax({
                type: "POST",
                url: "/_ajax/get_route/" + route + '/',
                data: data,
                async: false,
                cache: true,
                dataType: "json",
                success: function (data) {
                    response = data;
                    return true;
                },
                failure: function (errMsg) {
                    console.log(errMsg);
                }
            });
            return response.url;
        }
    };

    (function () {
        Lib.Tabs = function (options) {
            this.defaults = {
                tabs: '[js-tabs-tab]',
                panes: '[js-tabs-pane]',
                speed: 300,
                hash: false,
                openFirst: true,
                $parent: $('body')
            };
            this.options = $.extend({}, this.defaults, options);
            this.$tabs = (typeof this.options.tabs === 'string') ? $(this.options.tabs) : this.options.tabs;
            this.$panes = (typeof this.options.panes === 'string') ? $(this.options.panes) : this.options.panes;
            this.groups = {};
            this.init();
        };
        var proto = Lib.Tabs.prototype;


        proto.select = function (group, id, speed) {
            var self = this,
                $panes = self.getPanes(group),
                $pane = $panes.filter('[data-tabs-pane~="' + group + ':' + id + '"]'),
                $tabs = self.getTabs(group),
                $tab = $tabs.filter('[data-tabs-tab~="' + group + ':' + id + '"]');

            var speed = typeof speed == 'undefined' ? self.options.speed : speed;

            if ($pane.length) {
                $tabs.removeClass('is-active');
                $tab.removeClass('is-broken').addClass('is-active');
                $panes.removeClass('is-hidden').stop().slideUp(speed, function () {
                    $(this).addClass('is-hidden');
                });
                $pane.removeClass('is-hidden').stop().slideDown(speed, function () {
                    self.options.$parent.trigger({
                        type: 'tabChanged',
                        tab: $tab,
                        pane: $pane,
                        group: group,
                        id: id
                    });
                });

                self.options.$parent.trigger({
                    type: 'tabChange',
                    tab: $tab,
                    pane: $pane,
                    group: group,
                    id: id
                });
            }
            else {
                $tab.addClass('is-broken');
            }
        };
        proto.getPanes = function (group) {
            return this.$panes.filter('[data-tabs-pane*="' + group + ':"]');
        };
        proto.getTabs = function (group) {
            return this.$tabs.filter('[data-tabs-tab*="' + group + ':"]');
        };
        proto.grouping = function () {
            var self = this;

            var $tabs = self.$tabs,
                groups = self.groups;

            $tabs.each(function (i, item) {
                var $tab = $tabs.eq(i),
                    attr = $tab.data('tabs-tab').split(':'),
                    group = attr[0];
                if (typeof groups[group] == 'undefined') {
                    groups[group] = new Array();
                }
                groups[group].push(attr[1]);
                $tab.data('js-tabs', attr);
            });

            this.groups = groups;
        };
        proto.binding = function () {
            var self = this;

            this.$tabs.off('.js-tabs').on('click.js-tabs', function (e) {
                e.preventDefault();
                var $tab = $(this);
                if ($tab.hasClass('is-active')) return false;

                var attr = $(this).data('js-tabs'),
                    group = attr[0],
                    id = attr[1];

                self.select(group, id);
                if (self.options.hash) {
                    location.hash = group + ':' + id;
                }
            });
        };
        proto.parseHash = function () {
            return location.hash.substring(1).split(':');
        };

        proto.init = function () {
            var self = this,
                hash = self.parseHash();

            self.$panes.removeAttr('style');
            self.grouping();
            self.binding();

            if (self.options.hash) {
                $(window).on('hashchange', function (e) {
                    hash = self.parseHash();
                    if (self.groups[hash[0]]) {
                        self.select(hash[0], hash[1], 0);
                    }
                });
            }

            $.each(self.groups, function (group, id) {
                var $tabs = self.getTabs(group),
                    $panes = self.getPanes(group),
                    $active = $tabs.filter('.is-active');

                if (self.options.hash && hash.length && hash[0] == group) {
                    if ($tabs.filter('[data-tabs-tab~="' + group + ':' + hash[1] + '"]').is(':visible') && $panes.filter('[data-tabs-pane~="' + group + ':' + hash[1] + '"]').length) {
                        self.select(group, hash[1], 0);
                    }
                }
                else if ($active.length && $active.is(':visible')) {
                    self.select($active.data('js-tabs')[0], $active.data('js-tabs')[1], 0);
                }
                else if (self.options.openFirst) {
                    self.select($tabs.eq(0).data('js-tabs')[0], $tabs.eq(0).data('js-tabs')[1], 0);
                }
                else {
                    $tabs.removeClass('is-active');
                    $panes.addClass('is-hidden').hide();
                }
            });
        };
    })();

    window.$$ = Lib;
})();


(function () {

    var Helper = {
        createTabs: function (options) {
            return new $$.Tabs(options);
        },
        bindPopup: function (elems) {
            if (!$.fn.arcticmodal) return;

            var $elems = typeof elems === 'string' ? $(elems) : elems;

            $elems.off('.open-modal').on('click.open-modal', function (e) {
                e.preventDefault();
                $$.openModal($($(this).data('modal')), {
                    closeOnEsc: true,
                    closeOnOverlayClick: true,
                    afterOpen: function (modal, $modal) {
                        $$.init.formPlugins($modal);
                    }
                });
            });
        },
        bindModal: function (elems) {
            if (!$.fn.arcticmodal) return;

            var $elems = typeof elems === 'string' ? $(elems) : elems;

            $elems.off('.open-modal').on('click.open-modal', function (e) {
                e.preventDefault();
                $$.openModal($($(this).data('modal')), {
                    closeOnEsc: false,
                    closeOnOverlayClick: false,
                    beforeOpen: function (modal, $modal) {
                        $modal.addClass('animated animate-wait fadeInUp');
                    },
                    afterOpen: function (modal, $modal) {
                        $$.init.formPlugins($modal);
                    }
                });
            });
        }
    };

    window.Helper = Helper;
})();


$.fn.formHandle = function (options) {
    options = options || {};
    this.each(function () {
        $$.uniFormHandle($(this), options);
    });
    return this;
};


$.fn.selectus = function (options) {
    return $(this).each(function (i, select) {
        var $select = $(select);
        if ($select.data('selectus')) {
            $select.triggerHandler('update');
            return;
        }

        var $options = $select.children('option');
        var $outer = $('<div class="selectus"><div class="selectus-input"><input type="text" name="selectus_input" value=""><div class="selectus-drop"></div></div></div>');
        var $input = $outer.find('input').prop('disabled', true);
        var prefix = $select.data('selectus-prefix')
        // var value_start = $options.filter('[selected]').val() || $options.eq(0).val();

        if ($select.attr('disabled')) $outer.addClass('is-disabled');


        $select.replaceWith($outer);
        $select.prependTo($outer);
        $input.val($options.filter(':selected').text());

        $select.off('.selectus').on({
            'change.selectus silentChange.selectus': function (e) {
                var text = $options.filter(':selected').text();
                $input.val(text == prefix || !prefix ? text : prefix + ': ' + text);
            },
            'update.selectus': function (e) {
                $select = $(this);
                $outer = $select.closest('.selectus');
                $input = $outer.find('input');
                $options = $select.children('option');
                $select.trigger('silentChange');
            },
            'destroy.selectus': function (e) {
                $outer.replaceWith($select.off('.selectus').data('selectus', false));
            },
            'select.selectus': function (e, data) {
                if (!isNaN(data.id)) {
                    $options.eq(data.id).prop('selected', true);
                    $select.trigger('silentChange');
                }
                else if (data.value) {
                    $options.filter(function (i, opt) {
                        return $options.eq(i).attr('value') == data.value;
                    }).prop('selected', true);
                    $select.trigger('silentChange');
                }
            }
        }).data('selectus', true);

    });
};


(function ($) {
    $.fn.inputFile = function () {
        return $(this).each(function (parent_i, object) {
            var $input = $(object);

            if ($input.data('init')) return;
            $input.data('init', true);
            $input.wrap("<label class='input-file'> </label>");
            var NameInput = $input.attr('js-input-name');
            var Wrapper = '<div class="input-file__wrapper">' + '<div class="input-file__icon">' + '</div>' + '<div class="input-file__text">' + NameInput + '</div>' + '</div>';
            $input.after(Wrapper);
            $input.on('change', function () {
                var name = $(this).val();
                var nameArr = name.split('\\');
                if (nameArr.length > 1)
                    var name = nameArr[nameArr.length - 1];
                if (name != 0) {
                    $(this).next().remove();
                    var FieldNameWrapper = '<div class="input-file__wrapper">' + '<div class="input-file__icon">' + '</div>' + '<div class="input-file__text">' + name + '</div>' + '<div class="input-file__close">' + '</div>' + '</div>';
                    $(this).after(FieldNameWrapper);
                    $('.input-file__close').on('click', function () {
                        $(this).parent().prev().val("");
                        $(this).parent().prev().prop('disabled', false);
                        $(this).parent().html(Wrapper);
                        return false;
                    });
                }
                else {

                }
            });

        });
    };
    $.fn.inputShadow = function () {
        return $(this).each(function (parent_i, parent) {
            var $parent = $(parent);
            var $input = $parent.find('input, textarea');

            $parent.removeClass('is-focused is-filled');
            $input.off('.input-shadow').on({
                'focus.input-shadow': function (e) {
                    $parent.addClass('is-focused');
                },
                'blur.input-shadow': function (e) {
                    $parent.removeClass('is-focused');
                },
                'keyup.input-shadow': function (e) {
                    if ($parent.hasClass('is-error') && $input.val().length > 0) {
                        $parent.removeClass('is-error');
                    }
                },
                'change.input-shadow changeSilent.input-shadow': function (e) {
                    if ($input.val().length > 0) {
                        $parent.addClass('is-filled');
                    }
                    else {
                        $parent.removeClass('is-filled');
                    }
                }
            }).trigger('changeSilent');
        });
    };
})(jQuery);