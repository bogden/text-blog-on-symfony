
/**
 * Инициализация хелпера
 */
$(function(){
    $$.$body = $('body');

    $$.isMobile && $('html').addClass('is-mobile');

    var init = {
        fastclick: function (){
            $$.$body.find('a, button, input').on('touchstart', function (){});
        },
        sticky: function (context){
            if ( $$.isMobile || !$.fn.Stickyfill ) return;
            $('.sticky', context).Stickyfill();
        },
        popup: function (context){
            Helper.bindPopup($('[js-open-popup]', context));
        },
        modal: function (context){
            Helper.bindModal($('[js-open-modal]', context));
        },
        tabs: function (){
            Helper.createTabs();
        },
        formPlugins: function (context) {
            $('[js-selectus]', context).selectus();
            $('[js-input]', context).inputShadow();
            $('[js-input-files]', context).inputFile();
            $('[js-form]', context).formHandle({
                beforeData: function ($form) {},
                afterData: function () {},
                success: function (data, $form) {
                    if(data.reload_block){
                        $$.loader(data.reload_block);
                    }
                    if (data.clear_form) {
                        $form.resetForm();
                    }
                    if (data.alert) {
                        $$.openModal($('<div class="window__bg window__bg_odd"></div><h1>' + data.alert + '</h1>')).addClass('window');
                    }
                    if(data.message){
                        $.fancybox.open('<div class="message">'+data.message+'</div>');
                    }
                    if(data.errors){
                        console.log(data.errors);
                    }
                }.bind(this)
            });

            this.phoneMask(context);
            this.inputmask(context);
        },
        inputmask: function(context){
            $('[data-inputmask]', context).inputmask();
        },
        phoneMask: function(context){
            $('[data-masked="phone"]', context).inputmask("+7(999)-999-9999", {showMaskOnHover: false});
        }
    };

    init.all = function (context){
        $('[js-input-file]', context).inputFile();
        $$.init.formPlugins();
        $$.init.modal();
    };
    $$.init = init;

    $$.init.all();
});
