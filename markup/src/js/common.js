$(function () {
    var $body = $('body');
    $('[data-fancybox]').fancybox({
        onComplete: function () {
            $('.body__wrapper').css({
                'filter': 'blur(10px)'
            });
        },
        beforeClose: function () {
            $('.body__wrapper').css({
                'filter': 'blur(0px)'
            });
        }
    });
    $('[data-carousel]').each(function (e) {
        var $params = {
            items: 1,
            loop: !!$(this).data('loop'),
            dots: !!$(this).data('dots'),
            nav: !!$(this).data('navs'),
            autoheight: true,
            navText: [
                '<span class="ion-ios-arrow-thin-left"></span>',
                '<span class="ion-ios-arrow-thin-right"></span>',
            ],
            responsive : {
                768 : {
                    items: $(this).data('items'),
                }
            }
        };
        console.log($params);
        $(this).owlCarousel($params);
    });

    $(window).on('scroll',function(e){
        parallaxScroll();
    });

    function parallaxScroll(){
        var scrolled = $(window).scrollTop();
        $('[data-paralax]').each(function () {

        })
    }

    $('[data-toggle=popover]').popover();

    // Удаление комментариев

    $body.on('click','[data-delete-comment]', function (e) {
        e.preventDefault();
        var $this = $(this);
        $.ajax({
            type: "POST",
            url: $$.getRoute('comment-delete',[]),
            data: {
                'id': $this.attr('data-delete-comment')
            },
            dataType: "json",
            success: function(data){
                if(data.success){
                    $this.closest('[data-comment]').slideUp();
                }
                return true;
            },
            failure: function(errMsg) {
                console.log(errMsg);
            }
        });
    });

    // Удаление постов

    $body.on('click','[data-delete-post]', function (e) {
        e.preventDefault();
        var $this = $(this);
        $.ajax({
            type: "POST",
            url: $$.getRoute('delete-post',{slug: $this.attr('data-delete-post')}),
            data: {
                'slug': $this.attr('data-delete-post')
            },
            dataType: "json",
            success: function(data){
                if(data.success){
                    $this.closest('[data-post]').slideUp();
                }
                return true;
            },
            failure: function(errMsg) {
                console.log(errMsg);
            }
        });
    });
});