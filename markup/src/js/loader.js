/**
 * Created by bogde on 26.09.2017.
 */
$(function () {
    $$.loader = function (block) {
        var $blocks = block ? $('[data-loader-id='+ block +']') : $('[data-loader]');
        $blocks.each(function (e) {
            var $this = $(this);
            $this.find('.progress-bar').attr('aria-valuenow',0).width(0);
            $.ajax({
                type: "POST",
                url: $this.data('loader'),
                data: {
                    'post_id': $this.data('loader')
                },
                beforeSend: function () {
                    $this.find('.progress-bar').attr('aria-valuenow',50).css('width','50%');

                },
                success: function(data){
                    $this.find('.progress-bar').attr('aria-valuenow',100).css('width','100%');
                    setTimeout(function () {
                        $this.html(data)
                    },1000);
                    return true;
                },
                failure: function(errMsg) {
                    console.log(errMsg);
                }
            });
        })
    };
    $$.loader();

});